
workingDir='Media'
numFrames=24;
fileNames=cell(1,24)
fileNames{1,1}='img1.jpg';

for i=1:numFrames
    fileNames{i}=sprintf('img%d.jpg',i);
end

I = imread(fileNames{1});

sequence = zeros([size(I) numFrames],class(I));
sequence(:,:,:,1) = I;

% Create image sequence array
for p = 2:numFrames
    sequence(:,:,:,p) = imread(fileNames{p}); 
end


outputVideo = VideoWriter(fullfile(workingDir,'Parking.avi'));
outputVideo.FrameRate = 2;
open(outputVideo)

for ii = 1:numFrames
   img = imread(fullfile(workingDir,'images',fileNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)