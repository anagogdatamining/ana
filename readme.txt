
1. For each data sample of given format you need to run "preproc.m" just once

2. This will generate the table with header : Day,Month,Year,Hour,Minute,Longitude,Latitude

3. Next is the "wholeDay.m" function which takes as input parameters (Day,Month,Year)

4. This will generate 24 images in Media\Images folder, each for one hour starting from a noon on a date used as input

5. Run "video.m" to generate video from 24 images