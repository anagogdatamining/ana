clear all;

temp=importdata('data.csv',',');
textdata=temp.textdata;
numdata=temp.data;

clear temp;

header=textdata(1,:);
textdata(1,:)=[];

%convert string to date and time

instNum=length(textdata);

month=zeros(instNum,1);
day=zeros(instNum,1);
year=zeros(instNum,1);
hour=zeros(instNum,1);
minute=zeros(instNum,1);
err=0;


for i =1:instNum
    temp=textdata{i,2};
    ind=strfind(temp,'/');
    
    if size(ind)==[0,0]
        
        err=[err,i];
    else
        x=temp(1:(ind(1,1)-1));
        month(i,1)=str2double(x);
        
        x=temp((ind(1,1)+1):(ind(1,2)-1));
        day(i,1)=str2double(x);
        
        x=temp((ind(1,2)+1):(ind(1,2)+5));
        year(i,1)=str2double(x);
        
        ind1=strfind(temp,' ');
        ind2=strfind(temp,':');
        
        x=temp((ind1+1):(ind2-1));
        hour(i,1)=str2double(x);
        
        temp(1:ind2)=[];
        minute(i,1)=str2double(temp);
    end
end

temp=textdata(:,5);
ind=find(strcmp(temp,'Parked'));

long=str2double(textdata(ind,3));
lat=str2double(textdata(ind,4));
data=[month(ind) day(ind) year(ind) hour(ind) minute(ind) long lat ];

save ('firstSprint','data');

