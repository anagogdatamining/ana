function wholeDay(day, month, year)
% input parameters are x-day , y-month and z-year

load firstSprint.mat;
workingDir='Media';
bck=imread('bck2.png');
     
ind= find(data(:,2)==day);
temp1=data(ind,:);

ind=find(temp1(:,4)>=12);
temp1=temp1(ind,:);

ind=find(data(:,2)==day+1);
temp2=data(ind,:);

ind=find(temp2(:,4)<12);
temp2=temp2(ind,:);

temp=[temp1; temp2];

ind= find(temp(:,1)==month);
temp=temp(ind,:);

ind=find(temp(:,3)==year);
temp=temp(ind,:);


data_all=temp;

for h=12:24
    h
 ind=find(data_all(:,4)==h);
 if length(ind)>0
     
     data=data_all(ind,6:7);
     
     data(:,1)=data(:,1)-41268231;
     data(:,2)=data(:,2)-1965991;
     
     ind=find(data(:,1)<0);
     data(ind,:)=[];
     
     ind=find(data(:,2)<0);
     data(ind,:)=[];
     
     ind=find(data(:,1)>186523);
     data(ind,:)=[];
     
     ind=find(data(:,2)>210876);
     data(ind,:)=[];
     
     data(:,1)=data(:,1)*1.41;
     data=floor(data/100);
     
     
     pointmap=zeros(2630,2108);
     
     [m,n]=size(data);
     for i=1:m
         t1=2630-data(i,1);
         if t1==0
             t1=1;
         end
         t2=data(i,2);
         if t2==0
             t2=1;
         end
         pointmap(t1,t2)=pointmap(t1,t2)+1;
     end
     
     s=[0.25 0.5 0.25; 0.5 1 0.5; 0.25 0.5 0.25];
     y=pointmap;
     for i=1:10
         y=filter2(s,y);
     end
     
     for i=1:2630
         for j=1:2108
             if y(i,j)>255
                 y(i,j)=255;
             end
         end
     end
     
     y=uint8(y);
     
     x=y+1;
     x=255-x;
     
     text_str='';
     text_str=sprintf('%d/%d/%d  %d hours',day,month,year,h);
     
     f=figure,set(f, 'visible','off'), imshow(bck), hold on
     text(1200,30,text_str,'FontSize',12,'Color',[0 0 0]);
     hImg=imshow(x); set(hImg, 'AlphaData', y);
     colormap autumn;
     filename=sprintf('img%d',h-11);
     
     print(f, '-djpeg',fullfile(workingDir,'images',filename) );
 end
end

for h=0:11
    h
 ind=find(data_all(:,4)==h);
 if length(ind)>0
     
     data=data_all(ind,6:7);
     
     data(:,1)=data(:,1)-41268231;
     data(:,2)=data(:,2)-1965991;
     
     ind=find(data(:,1)<0);
     data(ind,:)=[];
     
     ind=find(data(:,2)<0);
     data(ind,:)=[];
     
     ind=find(data(:,1)>186523);
     data(ind,:)=[];
     
     ind=find(data(:,2)>210876);
     data(ind,:)=[];
     
     data(:,1)=data(:,1)*1.41;
     data=floor(data/100);
     
     
     pointmap=zeros(2630,2108);
     
     bck=imread('bck2.png');
     [m,n]=size(data);
     for i=1:m
         t1=2630-data(i,1);
         if t1==0
             t1=1;
         end
         t2=data(i,2);
         if t2==0
             t2=1;
         end
         pointmap(t1,t2)=pointmap(t1,t2)+1;
     end
     
     s=[0.25 0.5 0.25; 0.5 1 0.5; 0.25 0.5 0.25];
     y=pointmap;
     for i=1:10
         y=filter2(s,y);
     end
     
     for i=1:2630
         for j=1:2108
             if y(i,j)>255
                 y(i,j)=255;
             end
         end
     end
     
     y=uint8(y);
     
     x=y+1;
     x=255-x;
     
     text_str='';
     text_str=sprintf('%d/%d/%d  %d hours',day+1,month,year,h);
     
     f=figure,set(f, 'visible','off'), imshow(bck), hold on
     text(1200,30,text_str,'FontSize',12,'Color',[0 0 0]);
     hImg=imshow(x); set(hImg, 'AlphaData', y);
     colormap autumn;
     filename=sprintf('img%d',h+13);
     
     print(f, '-djpeg',fullfile(workingDir,'images',filename));
 end
end
close all;
end


